### RedisCli ###
A back to the basics R interface to redis for linux systems. It is based on redis-cli. 

* It allows passing of any commands to redis, in native redis syntax. 
* It provides a "pipeline" or batch processing to make for faster massive additions or requests
* It provides some shortcuts (such as saving the database, ...)

See the help file (?RedisCli in R) for more informations.

### Install it ###
To install it:

1. Make sure that redis-cli is installed (redis-tools package on ubuntu 16.10). 
2. Launch R in the redisCli folder
3. Make sure devtools is installed (install.packages("devtools"))
4. Document and install with devtools

    library(devtools)
    document()
    install()

### How to participate ###
Pull requests are welcome :-)
